package com.ruoyi.myweb.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 活动对象 activities
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class Activities extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 活动id */
    private Long activityId;

    /** 俱乐部id */
    @Excel(name = "俱乐部id")
    private Long clubId;

    /** 活动名 */
    @Excel(name = "活动名")
    private String activityName;

    /** 活动时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "活动时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activityTime;

    /** 活动地点 */
    @Excel(name = "活动地点")
    private String activityLocation;

    /** 活动描述 */
    @Excel(name = "活动描述")
    private String activityDescription;

    public void setActivityId(Long activityId) 
    {
        this.activityId = activityId;
    }

    public Long getActivityId() 
    {
        return activityId;
    }
    public void setClubId(Long clubId)
    {
        this.clubId = clubId;
    }

    public Long getClubId()
    {
        return clubId;
    }
    public void setActivityName(String activityName) 
    {
        this.activityName = activityName;
    }

    public String getActivityName() 
    {
        return activityName;
    }
    public void setActivityTime(Date activityTime) 
    {
        this.activityTime = activityTime;
    }

    public Date getActivityTime() 
    {
        return activityTime;
    }
    public void setActivityLocation(String activityLocation) 
    {
        this.activityLocation = activityLocation;
    }

    public String getActivityLocation() 
    {
        return activityLocation;
    }
    public void setActivityDescription(String activityDescription) 
    {
        this.activityDescription = activityDescription;
    }

    public String getActivityDescription() 
    {
        return activityDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("activityId", getActivityId())
            .append("clubId", getClubId())
            .append("activityName", getActivityName())
            .append("activityTime", getActivityTime())
            .append("activityLocation", getActivityLocation())
            .append("activityDescription", getActivityDescription())
            .toString();
    }
}
