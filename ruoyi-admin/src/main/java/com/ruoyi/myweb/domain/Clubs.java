package com.ruoyi.myweb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 俱乐部表对象 clubs
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class Clubs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 俱乐部id */
    private Long clubId;

    /** 俱乐部名称 */
    @Excel(name = "俱乐部名称")
    private String clubName;

    /** 俱乐部描述 */
    private String clubDescription;

    /** 管理员id */
    @Excel(name = "管理员id")
    private Long adminId;

    /** 俱乐部状态 */
    @Excel(name = "俱乐部状态")
    private String status;

    public void setClubId(Long clubId) 
    {
        this.clubId = clubId;
    }

    public Long getClubId() 
    {
        return clubId;
    }
    public void setClubName(String clubName) 
    {
        this.clubName = clubName;
    }

    public String getClubName() 
    {
        return clubName;
    }
    public void setClubDescription(String clubDescription) 
    {
        this.clubDescription = clubDescription;
    }

    public String getClubDescription() 
    {
        return clubDescription;
    }
    public void setAdminId(Long adminId) 
    {
        this.adminId = adminId;
    }

    public Long getAdminId() 
    {
        return adminId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("clubId", getClubId())
            .append("clubName", getClubName())
            .append("clubDescription", getClubDescription())
            .append("adminId", getAdminId())
            .append("status", getStatus())
            .toString();
    }
}
