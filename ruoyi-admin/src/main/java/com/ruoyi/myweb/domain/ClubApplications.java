package com.ruoyi.myweb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 俱乐部创建申请对象 club_applications
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class ClubApplications extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 申请表id */
    private Long applicationId;

    /** 俱乐部名 */
    @Excel(name = "俱乐部名")
    private String clubName;

    /** 申请人id */
    @Excel(name = "申请人id")
    private Long adminId;

    /** 申请状态 */
    @Excel(name = "申请状态")
    private String status;

    /** 驳回原因 */
    @Excel(name = "驳回原因")
    private String rejectionReason;

    public void setApplicationId(Long applicationId) 
    {
        this.applicationId = applicationId;
    }

    public Long getApplicationId() 
    {
        return applicationId;
    }
    public void setClubName(String clubName) 
    {
        this.clubName = clubName;
    }

    public String getClubName() 
    {
        return clubName;
    }
    public void setAdminId(Long adminId) 
    {
        this.adminId = adminId;
    }

    public Long getAdminId() 
    {
        return adminId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setRejectionReason(String rejectionReason) 
    {
        this.rejectionReason = rejectionReason;
    }

    public String getRejectionReason() 
    {
        return rejectionReason;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("applicationId", getApplicationId())
            .append("clubName", getClubName())
            .append("adminId", getAdminId())
            .append("status", getStatus())
            .append("rejectionReason", getRejectionReason())
            .toString();
    }
}
