package com.ruoyi.myweb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 角色对象 roles
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class Roles extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 角色表id */
    private Long roleId;

    /** 角色名称  */
    @Excel(name = "角色名称 ")
    private String roleName;

    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }
    public void setRoleName(String roleName) 
    {
        this.roleName = roleName;
    }

    public String getRoleName() 
    {
        return roleName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", getRoleId())
            .append("roleName", getRoleName())
            .toString();
    }
}
