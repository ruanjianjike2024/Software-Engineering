package com.ruoyi.myweb.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 俱乐部成员对象 club_members
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class ClubMembers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 成员表id */
    private Long memberId;

    /** 俱乐部id */
    @Excel(name = "俱乐部id")
    private Long clubId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 加入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "加入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date joinDate;

    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setClubId(Long clubId) 
    {
        this.clubId = clubId;
    }

    public Long getClubId() 
    {
        return clubId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setJoinDate(Date joinDate) 
    {
        this.joinDate = joinDate;
    }

    public Date getJoinDate() 
    {
        return joinDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memberId", getMemberId())
            .append("clubId", getClubId())
            .append("userId", getUserId())
            .append("joinDate", getJoinDate())
            .toString();
    }
}
