package com.ruoyi.myweb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 活动参与对象 activity_participations
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public class ActivityParticipations extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 参与表id */
    private Long participationId;

    /** 活动id */
    @Excel(name = "活动id")
    private Long activityId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 参与状态 */
    @Excel(name = "参与状态")
    private String status;

    public void setParticipationId(Long participationId) 
    {
        this.participationId = participationId;
    }

    public Long getParticipationId() 
    {
        return participationId;
    }
    public void setActivityId(Long activityId) 
    {
        this.activityId = activityId;
    }

    public Long getActivityId() 
    {
        return activityId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("participationId", getParticipationId())
            .append("activityId", getActivityId())
            .append("userId", getUserId())
            .append("status", getStatus())
            .toString();
    }
}
