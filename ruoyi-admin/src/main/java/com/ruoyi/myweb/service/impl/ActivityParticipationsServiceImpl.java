package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.ActivityParticipationsMapper;
import com.ruoyi.myweb.domain.ActivityParticipations;
import com.ruoyi.myweb.service.IActivityParticipationsService;

/**
 * 活动参与Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class ActivityParticipationsServiceImpl implements IActivityParticipationsService 
{
    @Autowired
    private ActivityParticipationsMapper activityParticipationsMapper;

    /**
     * 查询活动参与
     * 
     * @param participationId 活动参与主键
     * @return 活动参与
     */
    @Override
    public ActivityParticipations selectActivityParticipationsByParticipationId(Long participationId)
    {
        return activityParticipationsMapper.selectActivityParticipationsByParticipationId(participationId);
    }

    /**
     * 查询活动参与列表
     * 
     * @param activityParticipations 活动参与
     * @return 活动参与
     */
    @Override
    public List<ActivityParticipations> selectActivityParticipationsList(ActivityParticipations activityParticipations)
    {
        return activityParticipationsMapper.selectActivityParticipationsList(activityParticipations);
    }

    /**
     * 新增活动参与
     * 
     * @param activityParticipations 活动参与
     * @return 结果
     */
    @Override
    public int insertActivityParticipations(ActivityParticipations activityParticipations)
    {
        return activityParticipationsMapper.insertActivityParticipations(activityParticipations);
    }

    /**
     * 修改活动参与
     * 
     * @param activityParticipations 活动参与
     * @return 结果
     */
    @Override
    public int updateActivityParticipations(ActivityParticipations activityParticipations)
    {
        return activityParticipationsMapper.updateActivityParticipations(activityParticipations);
    }

    /**
     * 批量删除活动参与
     * 
     * @param participationIds 需要删除的活动参与主键
     * @return 结果
     */
    @Override
    public int deleteActivityParticipationsByParticipationIds(Long[] participationIds)
    {
        return activityParticipationsMapper.deleteActivityParticipationsByParticipationIds(participationIds);
    }

    /**
     * 删除活动参与信息
     * 
     * @param participationId 活动参与主键
     * @return 结果
     */
    @Override
    public int deleteActivityParticipationsByParticipationId(Long participationId)
    {
        return activityParticipationsMapper.deleteActivityParticipationsByParticipationId(participationId);
    }
}
