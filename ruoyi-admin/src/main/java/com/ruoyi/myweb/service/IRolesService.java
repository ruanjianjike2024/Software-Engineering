package com.ruoyi.myweb.service;

import java.util.List;
import com.ruoyi.myweb.domain.Roles;

/**
 * 角色Service接口
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public interface IRolesService 
{
    /**
     * 查询角色
     * 
     * @param roleId 角色主键
     * @return 角色
     */
    public Roles selectRolesByRoleId(Long roleId);

    /**
     * 查询角色列表
     * 
     * @param roles 角色
     * @return 角色集合
     */
    public List<Roles> selectRolesList(Roles roles);

    /**
     * 新增角色
     * 
     * @param roles 角色
     * @return 结果
     */
    public int insertRoles(Roles roles);

    /**
     * 修改角色
     * 
     * @param roles 角色
     * @return 结果
     */
    public int updateRoles(Roles roles);

    /**
     * 批量删除角色
     * 
     * @param roleIds 需要删除的角色主键集合
     * @return 结果
     */
    public int deleteRolesByRoleIds(Long[] roleIds);

    /**
     * 删除角色信息
     * 
     * @param roleId 角色主键
     * @return 结果
     */
    public int deleteRolesByRoleId(Long roleId);
}
