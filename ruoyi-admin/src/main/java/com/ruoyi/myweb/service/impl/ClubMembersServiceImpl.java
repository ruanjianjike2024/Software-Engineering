package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.ClubMembersMapper;
import com.ruoyi.myweb.domain.ClubMembers;
import com.ruoyi.myweb.service.IClubMembersService;

/**
 * 俱乐部成员Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class ClubMembersServiceImpl implements IClubMembersService 
{
    @Autowired
    private ClubMembersMapper clubMembersMapper;

    /**
     * 查询俱乐部成员
     * 
     * @param memberId 俱乐部成员主键
     * @return 俱乐部成员
     */
    @Override
    public ClubMembers selectClubMembersByMemberId(Long memberId)
    {
        return clubMembersMapper.selectClubMembersByMemberId(memberId);
    }

    /**
     * 查询俱乐部成员列表
     * 
     * @param clubMembers 俱乐部成员
     * @return 俱乐部成员
     */
    @Override
    public List<ClubMembers> selectClubMembersList(ClubMembers clubMembers)
    {
        return clubMembersMapper.selectClubMembersList(clubMembers);
    }

    /**
     * 新增俱乐部成员
     * 
     * @param clubMembers 俱乐部成员
     * @return 结果
     */
    @Override
    public int insertClubMembers(ClubMembers clubMembers)
    {
        return clubMembersMapper.insertClubMembers(clubMembers);
    }

    /**
     * 修改俱乐部成员
     * 
     * @param clubMembers 俱乐部成员
     * @return 结果
     */
    @Override
    public int updateClubMembers(ClubMembers clubMembers)
    {
        return clubMembersMapper.updateClubMembers(clubMembers);
    }

    /**
     * 批量删除俱乐部成员
     * 
     * @param memberIds 需要删除的俱乐部成员主键
     * @return 结果
     */
    @Override
    public int deleteClubMembersByMemberIds(Long[] memberIds)
    {
        return clubMembersMapper.deleteClubMembersByMemberIds(memberIds);
    }

    /**
     * 删除俱乐部成员信息
     * 
     * @param memberId 俱乐部成员主键
     * @return 结果
     */
    @Override
    public int deleteClubMembersByMemberId(Long memberId)
    {
        return clubMembersMapper.deleteClubMembersByMemberId(memberId);
    }
}
