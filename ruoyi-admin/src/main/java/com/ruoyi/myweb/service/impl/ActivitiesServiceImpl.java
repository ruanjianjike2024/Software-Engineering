package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.ActivitiesMapper;
import com.ruoyi.myweb.domain.Activities;
import com.ruoyi.myweb.service.IActivitiesService;

/**
 * 活动Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class ActivitiesServiceImpl implements IActivitiesService 
{
    @Autowired
    private ActivitiesMapper activitiesMapper;

    /**
     * 查询活动
     * 
     * @param activityId 活动主键
     * @return 活动
     */
    @Override
    public Activities selectActivitiesByActivityId(Long activityId)
    {
        return activitiesMapper.selectActivitiesByActivityId(activityId);
    }

    /**
     * 查询活动列表
     * 
     * @param activities 活动
     * @return 活动
     */
    @Override
    public List<Activities> selectActivitiesList(Activities activities)
    {
        System.out.println(activitiesMapper.selectActivitiesList(activities));
        return activitiesMapper.selectActivitiesList(activities);
    }

    /**
     * 新增活动
     * 
     * @param activities 活动
     * @return 结果
     */
    @Override
    public int insertActivities(Activities activities)
    {
        return activitiesMapper.insertActivities(activities);
    }

    /**
     * 修改活动
     * 
     * @param activities 活动
     * @return 结果
     */
    @Override
    public int updateActivities(Activities activities)
    {
        return activitiesMapper.updateActivities(activities);
    }

    /**
     * 批量删除活动
     * 
     * @param activityIds 需要删除的活动主键
     * @return 结果
     */
    @Override
    public int deleteActivitiesByActivityIds(Long[] activityIds)
    {
        return activitiesMapper.deleteActivitiesByActivityIds(activityIds);
    }

    /**
     * 删除活动信息
     * 
     * @param activityId 活动主键
     * @return 结果
     */
    @Override
    public int deleteActivitiesByActivityId(Long activityId)
    {
        return activitiesMapper.deleteActivitiesByActivityId(activityId);
    }
}
