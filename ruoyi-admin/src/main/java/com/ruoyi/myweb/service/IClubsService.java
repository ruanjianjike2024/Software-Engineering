package com.ruoyi.myweb.service;

import java.util.List;
import com.ruoyi.myweb.domain.Clubs;

/**
 * 俱乐部表Service接口
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public interface IClubsService 
{
    /**
     * 查询俱乐部表
     * 
     * @param clubId 俱乐部表主键
     * @return 俱乐部表
     */
    public Clubs selectClubsByClubId(Long clubId);

    /**
     * 查询俱乐部表列表
     * 
     * @param clubs 俱乐部表
     * @return 俱乐部表集合
     */
    public List<Clubs> selectClubsList(Clubs clubs);

    /**
     * 新增俱乐部表
     * 
     * @param clubs 俱乐部表
     * @return 结果
     */
    public int insertClubs(Clubs clubs);

    /**
     * 修改俱乐部表
     * 
     * @param clubs 俱乐部表
     * @return 结果
     */
    public int updateClubs(Clubs clubs);

    /**
     * 批量删除俱乐部表
     * 
     * @param clubIds 需要删除的俱乐部表主键集合
     * @return 结果
     */
    public int deleteClubsByClubIds(Long[] clubIds);

    /**
     * 删除俱乐部表信息
     * 
     * @param clubId 俱乐部表主键
     * @return 结果
     */
    public int deleteClubsByClubId(Long clubId);
}
