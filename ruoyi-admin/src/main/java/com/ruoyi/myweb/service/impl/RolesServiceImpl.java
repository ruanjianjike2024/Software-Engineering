package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.RolesMapper;
import com.ruoyi.myweb.domain.Roles;
import com.ruoyi.myweb.service.IRolesService;

/**
 * 角色Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class RolesServiceImpl implements IRolesService 
{
    @Autowired
    private RolesMapper rolesMapper;

    /**
     * 查询角色
     * 
     * @param roleId 角色主键
     * @return 角色
     */
    @Override
    public Roles selectRolesByRoleId(Long roleId)
    {
        return rolesMapper.selectRolesByRoleId(roleId);
    }

    /**
     * 查询角色列表
     * 
     * @param roles 角色
     * @return 角色
     */
    @Override
    public List<Roles> selectRolesList(Roles roles)
    {
        return rolesMapper.selectRolesList(roles);
    }

    /**
     * 新增角色
     * 
     * @param roles 角色
     * @return 结果
     */
    @Override
    public int insertRoles(Roles roles)
    {
        return rolesMapper.insertRoles(roles);
    }

    /**
     * 修改角色
     * 
     * @param roles 角色
     * @return 结果
     */
    @Override
    public int updateRoles(Roles roles)
    {
        return rolesMapper.updateRoles(roles);
    }

    /**
     * 批量删除角色
     * 
     * @param roleIds 需要删除的角色主键
     * @return 结果
     */
    @Override
    public int deleteRolesByRoleIds(Long[] roleIds)
    {
        return rolesMapper.deleteRolesByRoleIds(roleIds);
    }

    /**
     * 删除角色信息
     * 
     * @param roleId 角色主键
     * @return 结果
     */
    @Override
    public int deleteRolesByRoleId(Long roleId)
    {
        return rolesMapper.deleteRolesByRoleId(roleId);
    }
}
