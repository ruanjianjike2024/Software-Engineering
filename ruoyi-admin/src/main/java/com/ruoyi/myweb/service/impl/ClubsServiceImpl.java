package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.ClubsMapper;
import com.ruoyi.myweb.domain.Clubs;
import com.ruoyi.myweb.service.IClubsService;

/**
 * 俱乐部表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class ClubsServiceImpl implements IClubsService 
{
    @Autowired
    private ClubsMapper clubsMapper;

    /**
     * 查询俱乐部表
     * 
     * @param clubId 俱乐部表主键
     * @return 俱乐部表
     */
    @Override
    public Clubs selectClubsByClubId(Long clubId)
    {
        return clubsMapper.selectClubsByClubId(clubId);
    }

    /**
     * 查询俱乐部表列表
     * 
     * @param clubs 俱乐部表
     * @return 俱乐部表
     */
    @Override
    public List<Clubs> selectClubsList(Clubs clubs)
    {
        return clubsMapper.selectClubsList(clubs);
    }

    /**
     * 新增俱乐部表
     * 
     * @param clubs 俱乐部表
     * @return 结果
     */
    @Override
    public int insertClubs(Clubs clubs)
    {
        return clubsMapper.insertClubs(clubs);
    }

    /**
     * 修改俱乐部表
     * 
     * @param clubs 俱乐部表
     * @return 结果
     */
    @Override
    public int updateClubs(Clubs clubs)
    {
        return clubsMapper.updateClubs(clubs);
    }

    /**
     * 批量删除俱乐部表
     * 
     * @param clubIds 需要删除的俱乐部表主键
     * @return 结果
     */
    @Override
    public int deleteClubsByClubIds(Long[] clubIds)
    {
        return clubsMapper.deleteClubsByClubIds(clubIds);
    }

    /**
     * 删除俱乐部表信息
     * 
     * @param clubId 俱乐部表主键
     * @return 结果
     */
    @Override
    public int deleteClubsByClubId(Long clubId)
    {
        return clubsMapper.deleteClubsByClubId(clubId);
    }
}
