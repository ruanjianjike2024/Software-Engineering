package com.ruoyi.myweb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.myweb.mapper.ClubApplicationsMapper;
import com.ruoyi.myweb.domain.ClubApplications;
import com.ruoyi.myweb.service.IClubApplicationsService;

/**
 * 俱乐部创建申请Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@Service
public class ClubApplicationsServiceImpl implements IClubApplicationsService 
{
    @Autowired
    private ClubApplicationsMapper clubApplicationsMapper;

    /**
     * 查询俱乐部创建申请
     * 
     * @param applicationId 俱乐部创建申请主键
     * @return 俱乐部创建申请
     */
    @Override
    public ClubApplications selectClubApplicationsByApplicationId(Long applicationId)
    {
        return clubApplicationsMapper.selectClubApplicationsByApplicationId(applicationId);
    }

    /**
     * 查询俱乐部创建申请列表
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 俱乐部创建申请
     */
    @Override
    public List<ClubApplications> selectClubApplicationsList(ClubApplications clubApplications)
    {
        return clubApplicationsMapper.selectClubApplicationsList(clubApplications);
    }

    /**
     * 新增俱乐部创建申请
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 结果
     */
    @Override
    public int insertClubApplications(ClubApplications clubApplications)
    {
        return clubApplicationsMapper.insertClubApplications(clubApplications);
    }

    /**
     * 修改俱乐部创建申请
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 结果
     */
    @Override
    public int updateClubApplications(ClubApplications clubApplications)
    {
        return clubApplicationsMapper.updateClubApplications(clubApplications);
    }

    /**
     * 批量删除俱乐部创建申请
     * 
     * @param applicationIds 需要删除的俱乐部创建申请主键
     * @return 结果
     */
    @Override
    public int deleteClubApplicationsByApplicationIds(Long[] applicationIds)
    {
        return clubApplicationsMapper.deleteClubApplicationsByApplicationIds(applicationIds);
    }

    /**
     * 删除俱乐部创建申请信息
     * 
     * @param applicationId 俱乐部创建申请主键
     * @return 结果
     */
    @Override
    public int deleteClubApplicationsByApplicationId(Long applicationId)
    {
        return clubApplicationsMapper.deleteClubApplicationsByApplicationId(applicationId);
    }
}
