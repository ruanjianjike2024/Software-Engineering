package com.ruoyi.myweb.service;

import java.util.List;
import com.ruoyi.myweb.domain.ClubApplications;

/**
 * 俱乐部创建申请Service接口
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public interface IClubApplicationsService 
{
    /**
     * 查询俱乐部创建申请
     * 
     * @param applicationId 俱乐部创建申请主键
     * @return 俱乐部创建申请
     */
    public ClubApplications selectClubApplicationsByApplicationId(Long applicationId);

    /**
     * 查询俱乐部创建申请列表
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 俱乐部创建申请集合
     */
    public List<ClubApplications> selectClubApplicationsList(ClubApplications clubApplications);

    /**
     * 新增俱乐部创建申请
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 结果
     */
    public int insertClubApplications(ClubApplications clubApplications);

    /**
     * 修改俱乐部创建申请
     * 
     * @param clubApplications 俱乐部创建申请
     * @return 结果
     */
    public int updateClubApplications(ClubApplications clubApplications);

    /**
     * 批量删除俱乐部创建申请
     * 
     * @param applicationIds 需要删除的俱乐部创建申请主键集合
     * @return 结果
     */
    public int deleteClubApplicationsByApplicationIds(Long[] applicationIds);

    /**
     * 删除俱乐部创建申请信息
     * 
     * @param applicationId 俱乐部创建申请主键
     * @return 结果
     */
    public int deleteClubApplicationsByApplicationId(Long applicationId);
}
