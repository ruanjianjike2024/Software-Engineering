package com.ruoyi.myweb.mapper;

import java.util.List;
import com.ruoyi.myweb.domain.ClubMembers;

/**
 * 俱乐部成员Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public interface ClubMembersMapper 
{
    /**
     * 查询俱乐部成员
     * 
     * @param memberId 俱乐部成员主键
     * @return 俱乐部成员
     */
    public ClubMembers selectClubMembersByMemberId(Long memberId);

    /**
     * 查询俱乐部成员列表
     * 
     * @param clubMembers 俱乐部成员
     * @return 俱乐部成员集合
     */
    public List<ClubMembers> selectClubMembersList(ClubMembers clubMembers);

    /**
     * 新增俱乐部成员
     * 
     * @param clubMembers 俱乐部成员
     * @return 结果
     */
    public int insertClubMembers(ClubMembers clubMembers);

    /**
     * 修改俱乐部成员
     * 
     * @param clubMembers 俱乐部成员
     * @return 结果
     */
    public int updateClubMembers(ClubMembers clubMembers);

    /**
     * 删除俱乐部成员
     * 
     * @param memberId 俱乐部成员主键
     * @return 结果
     */
    public int deleteClubMembersByMemberId(Long memberId);

    /**
     * 批量删除俱乐部成员
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteClubMembersByMemberIds(Long[] memberIds);
}
