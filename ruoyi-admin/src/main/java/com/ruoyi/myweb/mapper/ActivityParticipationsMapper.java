package com.ruoyi.myweb.mapper;

import java.util.List;
import com.ruoyi.myweb.domain.ActivityParticipations;

/**
 * 活动参与Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
public interface ActivityParticipationsMapper 
{
    /**
     * 查询活动参与
     * 
     * @param participationId 活动参与主键
     * @return 活动参与
     */
    public ActivityParticipations selectActivityParticipationsByParticipationId(Long participationId);

    /**
     * 查询活动参与列表
     * 
     * @param activityParticipations 活动参与
     * @return 活动参与集合
     */
    public List<ActivityParticipations> selectActivityParticipationsList(ActivityParticipations activityParticipations);

    /**
     * 新增活动参与
     * 
     * @param activityParticipations 活动参与
     * @return 结果
     */
    public int insertActivityParticipations(ActivityParticipations activityParticipations);

    /**
     * 修改活动参与
     * 
     * @param activityParticipations 活动参与
     * @return 结果
     */
    public int updateActivityParticipations(ActivityParticipations activityParticipations);

    /**
     * 删除活动参与
     * 
     * @param participationId 活动参与主键
     * @return 结果
     */
    public int deleteActivityParticipationsByParticipationId(Long participationId);

    /**
     * 批量删除活动参与
     * 
     * @param participationIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteActivityParticipationsByParticipationIds(Long[] participationIds);
}
