package com.ruoyi.myweb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.myweb.domain.ActivityParticipations;
import com.ruoyi.myweb.service.IActivityParticipationsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动参与Controller
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/participations/participations")
public class ActivityParticipationsController extends BaseController
{
    @Autowired
    private IActivityParticipationsService activityParticipationsService;

    /**
     * 查询活动参与列表
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActivityParticipations activityParticipations)
    {
        startPage();
        List<ActivityParticipations> list = activityParticipationsService.selectActivityParticipationsList(activityParticipations);
        return getDataTable(list);
    }

    /**
     * 导出活动参与列表
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:export')")
    @Log(title = "活动参与", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ActivityParticipations activityParticipations)
    {
        List<ActivityParticipations> list = activityParticipationsService.selectActivityParticipationsList(activityParticipations);
        ExcelUtil<ActivityParticipations> util = new ExcelUtil<ActivityParticipations>(ActivityParticipations.class);
        util.exportExcel(response, list, "活动参与数据");
    }

    /**
     * 获取活动参与详细信息
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:query')")
    @GetMapping(value = "/{participationId}")
    public AjaxResult getInfo(@PathVariable("participationId") Long participationId)
    {
        return success(activityParticipationsService.selectActivityParticipationsByParticipationId(participationId));
    }

    /**
     * 新增活动参与
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:add')")
    @Log(title = "活动参与", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActivityParticipations activityParticipations)
    {
        return toAjax(activityParticipationsService.insertActivityParticipations(activityParticipations));
    }

    /**
     * 修改活动参与
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:edit')")
    @Log(title = "活动参与", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActivityParticipations activityParticipations)
    {
        return toAjax(activityParticipationsService.updateActivityParticipations(activityParticipations));
    }

    /**
     * 删除活动参与
     */
    @PreAuthorize("@ss.hasPermi('participations:participations:remove')")
    @Log(title = "活动参与", businessType = BusinessType.DELETE)
	@DeleteMapping("/{participationIds}")
    public AjaxResult remove(@PathVariable Long[] participationIds)
    {
        return toAjax(activityParticipationsService.deleteActivityParticipationsByParticipationIds(participationIds));
    }
}
