package com.ruoyi.myweb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.myweb.domain.ClubApplications;
import com.ruoyi.myweb.service.IClubApplicationsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 俱乐部创建申请Controller
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/applications/applications")
public class ClubApplicationsController extends BaseController
{
    @Autowired
    private IClubApplicationsService clubApplicationsService;

    /**
     * 查询俱乐部创建申请列表
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClubApplications clubApplications)
    {
        startPage();
        List<ClubApplications> list = clubApplicationsService.selectClubApplicationsList(clubApplications);
        return getDataTable(list);
    }

    /**
     * 导出俱乐部创建申请列表
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:export')")
    @Log(title = "俱乐部创建申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClubApplications clubApplications)
    {
        List<ClubApplications> list = clubApplicationsService.selectClubApplicationsList(clubApplications);
        ExcelUtil<ClubApplications> util = new ExcelUtil<ClubApplications>(ClubApplications.class);
        util.exportExcel(response, list, "俱乐部创建申请数据");
    }

    /**
     * 获取俱乐部创建申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:query')")
    @GetMapping(value = "/{applicationId}")
    public AjaxResult getInfo(@PathVariable("applicationId") Long applicationId)
    {
        return success(clubApplicationsService.selectClubApplicationsByApplicationId(applicationId));
    }

    /**
     * 新增俱乐部创建申请
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:add')")
    @Log(title = "俱乐部创建申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClubApplications clubApplications)
    {
        return toAjax(clubApplicationsService.insertClubApplications(clubApplications));
    }

    /**
     * 修改俱乐部创建申请
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:edit')")
    @Log(title = "俱乐部创建申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClubApplications clubApplications)
    {
        return toAjax(clubApplicationsService.updateClubApplications(clubApplications));
    }

    /**
     * 删除俱乐部创建申请
     */
    @PreAuthorize("@ss.hasPermi('applications:applications:remove')")
    @Log(title = "俱乐部创建申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{applicationIds}")
    public AjaxResult remove(@PathVariable Long[] applicationIds)
    {
        return toAjax(clubApplicationsService.deleteClubApplicationsByApplicationIds(applicationIds));
    }
}
