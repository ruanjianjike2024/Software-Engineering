package com.ruoyi.myweb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.myweb.domain.ClubMembers;
import com.ruoyi.myweb.service.IClubMembersService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 俱乐部成员Controller
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/members/members")
public class ClubMembersController extends BaseController
{
    @Autowired
    private IClubMembersService clubMembersService;

    /**
     * 查询俱乐部成员列表
     */
    @PreAuthorize("@ss.hasPermi('members:members:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClubMembers clubMembers)
    {
        startPage();
        List<ClubMembers> list = clubMembersService.selectClubMembersList(clubMembers);
        return getDataTable(list);
    }

    /**
     * 导出俱乐部成员列表
     */
    @PreAuthorize("@ss.hasPermi('members:members:export')")
    @Log(title = "俱乐部成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClubMembers clubMembers)
    {
        List<ClubMembers> list = clubMembersService.selectClubMembersList(clubMembers);
        ExcelUtil<ClubMembers> util = new ExcelUtil<ClubMembers>(ClubMembers.class);
        util.exportExcel(response, list, "俱乐部成员数据");
    }

    /**
     * 获取俱乐部成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('members:members:query')")
    @GetMapping(value = "/{memberId}")
    public AjaxResult getInfo(@PathVariable("memberId") Long memberId)
    {
        return success(clubMembersService.selectClubMembersByMemberId(memberId));
    }

    /**
     * 新增俱乐部成员
     */
    @PreAuthorize("@ss.hasPermi('members:members:add')")
    @Log(title = "俱乐部成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClubMembers clubMembers)
    {
        return toAjax(clubMembersService.insertClubMembers(clubMembers));
    }

    /**
     * 修改俱乐部成员
     */
    @PreAuthorize("@ss.hasPermi('members:members:edit')")
    @Log(title = "俱乐部成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClubMembers clubMembers)
    {
        return toAjax(clubMembersService.updateClubMembers(clubMembers));
    }

    /**
     * 删除俱乐部成员
     */
    @PreAuthorize("@ss.hasPermi('members:members:remove')")
    @Log(title = "俱乐部成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{memberIds}")
    public AjaxResult remove(@PathVariable Long[] memberIds)
    {
        return toAjax(clubMembersService.deleteClubMembersByMemberIds(memberIds));
    }
}
