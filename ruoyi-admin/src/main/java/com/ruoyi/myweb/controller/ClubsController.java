package com.ruoyi.myweb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.myweb.domain.Clubs;
import com.ruoyi.myweb.service.IClubsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 俱乐部表Controller
 * 
 * @author ruoyi
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/clubs/clubs")
public class ClubsController extends BaseController
{
    @Autowired
    private IClubsService clubsService;

    /**
     * 查询俱乐部表列表
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:list')")
    @GetMapping("/list")
    public TableDataInfo list(Clubs clubs)
    {
        startPage();
        List<Clubs> list = clubsService.selectClubsList(clubs);
        return getDataTable(list);
    }

    /**
     * 导出俱乐部表列表
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:export')")
    @Log(title = "俱乐部表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Clubs clubs)
    {
        List<Clubs> list = clubsService.selectClubsList(clubs);
        ExcelUtil<Clubs> util = new ExcelUtil<Clubs>(Clubs.class);
        util.exportExcel(response, list, "俱乐部表数据");
    }

    /**
     * 获取俱乐部表详细信息
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:query')")
    @GetMapping(value = "/{clubId}")
    public AjaxResult getInfo(@PathVariable("clubId") Long clubId)
    {
        return success(clubsService.selectClubsByClubId(clubId));
    }

    /**
     * 新增俱乐部表
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:add')")
    @Log(title = "俱乐部表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Clubs clubs)
    {
        return toAjax(clubsService.insertClubs(clubs));
    }

    /**
     * 修改俱乐部表
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:edit')")
    @Log(title = "俱乐部表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Clubs clubs)
    {
        return toAjax(clubsService.updateClubs(clubs));
    }

    /**
     * 删除俱乐部表
     */
    @PreAuthorize("@ss.hasPermi('clubs:clubs:remove')")
    @Log(title = "俱乐部表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{clubIds}")
    public AjaxResult remove(@PathVariable Long[] clubIds)
    {
        return toAjax(clubsService.deleteClubsByClubIds(clubIds));
    }
}
