import request from '@/utils/request'

// 查询活动参与列表
export function listParticipations(query) {
  return request({
    url: '/participations/participations/list',
    method: 'get',
    params: query
  })
}

// 查询活动参与详细
export function getParticipations(participationId) {
  return request({
    url: '/participations/participations/' + participationId,
    method: 'get'
  })
}

// 新增活动参与
export function addParticipations(data) {
  return request({
    url: '/participations/participations',
    method: 'post',
    data: data
  })
}

// 修改活动参与
export function updateParticipations(data) {
  return request({
    url: '/participations/participations',
    method: 'put',
    data: data
  })
}

// 删除活动参与
export function delParticipations(participationId) {
  return request({
    url: '/participations/participations/' + participationId,
    method: 'delete'
  })
}
