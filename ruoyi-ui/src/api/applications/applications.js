import request from '@/utils/request'

// 查询俱乐部创建申请列表
export function listApplications(query) {
  return request({
    url: '/applications/applications/list',
    method: 'get',
    params: query
  })
}

// 查询俱乐部创建申请详细
export function getApplications(applicationId) {
  return request({
    url: '/applications/applications/' + applicationId,
    method: 'get'
  })
}

// 新增俱乐部创建申请
export function addApplications(data) {
  return request({
    url: '/applications/applications',
    method: 'post',
    data: data
  })
}

// 修改俱乐部创建申请
export function updateApplications(data) {
  return request({
    url: '/applications/applications',
    method: 'put',
    data: data
  })
}

// 删除俱乐部创建申请
export function delApplications(applicationId) {
  return request({
    url: '/applications/applications/' + applicationId,
    method: 'delete'
  })
}
