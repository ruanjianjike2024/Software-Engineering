import request from '@/utils/request'

// 查询俱乐部表列表
export function listClubs(query) {
  return request({
    url: '/clubs/clubs/list',
    method: 'get',
    params: query
  })
}

// 查询俱乐部表详细
export function getClubs(clubId) {
  return request({
    url: '/clubs/clubs/' + clubId,
    method: 'get'
  })
}

// 新增俱乐部表
export function addClubs(data) {
  return request({
    url: '/clubs/clubs',
    method: 'post',
    data: data
  })
}

// 修改俱乐部表
export function updateClubs(data) {
  return request({
    url: '/clubs/clubs',
    method: 'put',
    data: data
  })
}

// 删除俱乐部表
export function delClubs(clubId) {
  return request({
    url: '/clubs/clubs/' + clubId,
    method: 'delete'
  })
}
