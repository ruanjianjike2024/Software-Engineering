import request from '@/utils/request'

// 查询俱乐部成员列表
export function listMembers(query) {
  return request({
    url: '/members/members/list',
    method: 'get',
    params: query
  })
}

// 查询俱乐部成员详细
export function getMembers(memberId) {
  return request({
    url: '/members/members/' + memberId,
    method: 'get'
  })
}

// 新增俱乐部成员
export function addMembers(data) {
  return request({
    url: '/members/members',
    method: 'post',
    data: data
  })
}

// 修改俱乐部成员
export function updateMembers(data) {
  return request({
    url: '/members/members',
    method: 'put',
    data: data
  })
}

// 删除俱乐部成员
export function delMembers(memberId) {
  return request({
    url: '/members/members/' + memberId,
    method: 'delete'
  })
}
