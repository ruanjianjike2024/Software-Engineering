import request from '@/utils/request'

// 查询角色列表
export function listRoles(query) {
  return request({
    url: '/roles/roles/list',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getRoles(roleId) {
  return request({
    url: '/roles/roles/' + roleId,
    method: 'get'
  })
}

// 新增角色
export function addRoles(data) {
  return request({
    url: '/roles/roles',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRoles(data) {
  return request({
    url: '/roles/roles',
    method: 'put',
    data: data
  })
}

// 删除角色
export function delRoles(roleId) {
  return request({
    url: '/roles/roles/' + roleId,
    method: 'delete'
  })
}
